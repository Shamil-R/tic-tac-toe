package core

type (
	// Стркутура хода (проставления символа)
	Play struct {
		// Имя игрока сделавшего ход
		Player string `json:"player"`
		// Флаг победного хода
		IsWin bool `json:"is_win"`
		// Позиция, на которой сделал ход игрок
		Position int `json:"position"`
	}
)

// Инициализация хода
func NewPlay(player string, position int) *Play {
	return &Play{
		Player:   player,
		Position: position,
	}
}
