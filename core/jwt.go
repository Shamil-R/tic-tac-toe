package core

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"time"
)

const (
	// Время жизни токена - для простоты долгоживущий
	ExpiredAt = 2027
	SecretKey = "cysti%&V#R&^V*%E&EDSA%DEAD"
	Issuer    = "tic-tak-toe"
	// Для простоты считаем, что игрока 2
	ClientNameRafael = "Rafael"
	ClientNameRodjer = "Rodjer"
)

type (
	JwtClaims struct {
		jwt.StandardClaims
		Client string `json:"client"`
	}

	// Api не должно быть публичным
	// Конфигурация JWT
	JwtConfig struct {
		Issuer    string `json:"issuer"`
		ExpiredAt int64  `yaml:"expiredAt"`
		SecretKey string `yaml:"secretKey"`
		Services  []string
	}
)

func NewJwtClaims(client string) *JwtClaims {
	return &JwtClaims{
		StandardClaims: jwt.StandardClaims{
			Issuer:    Issuer,
			ExpiresAt: time.Date(ExpiredAt, 1, 1, 0, 0, 0, 0, time.UTC).Unix(),
		},
		Client: client,
	}
}

func NewJwtConfig() *JwtConfig {
	return &JwtConfig{
		Issuer:    Issuer,
		ExpiredAt: ExpiredAt,
		SecretKey: SecretKey,
		Services:  []string{ClientNameRafael, ClientNameRodjer},
	}
}

// Создание JWT token
func (c *JwtConfig) CreateToken(claims jwt.Claims) (*string, error) {
	if c.SecretKey == "" {
		return nil, errors.New("Secret key is empty")
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(c.SecretKey))
	if err != nil {
		return nil, errors.New("Can't create jwt token")
	}
	return &tokenString, nil
}

// CreateJwtToken Создать токен
func CreateJwtToken(client string) (*string, error) {
	claims := NewJwtClaims(client)
	j := NewJwtConfig()
	return j.CreateToken(claims)
}
