package core

import (
	"fmt"
	"testing"
)

func TestCreateCatalogJwtToken(t *testing.T) {
	token, e := CreateJwtToken("Rafael")
	if e != nil {
		t.Fatal(e.Error())
	}

	fmt.Println(*token)
}
