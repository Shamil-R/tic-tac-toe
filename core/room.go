package core

import (
	"errors"
)

type (
	Room struct {
		Id    int           `json:"id"`
		Games map[int]*Game `json:"games"`
		Users []*User       `json:"-"`
	}
)

var Rooms = map[int]*Room{}
var LastRoomId int

func NewRoom(lastRoomId int, username string) *Room {
	return &Room{
		Id:    lastRoomId,
		Users: []*User{GetUserByName(username)},
	}
}

func CreateRoom(userName string) (id int, err error) {
	LastRoomId += 1
	Rooms[LastRoomId] = NewRoom(LastRoomId, userName)
	id = LastRoomId
	return
}

func JoinRoom(id int, userName string) (err error) {
	if _, ok := Rooms[id]; !ok {
		err = errors.New("Room not found")
		return
	}
	for _, user := range Rooms[id].Users {
		if user.Name == userName {
			err = errors.New("you already in room")
			return
		}
	}
	Rooms[id].Users = append(Rooms[id].Users, GetUserByName(userName))
	return
}

func GetRooms(userName string) (res []*Room) {
	res = make([]*Room, 0)
	for _, room := range Rooms {
		for _, user := range room.Users {
			if user.Name == userName {
				res = append(res, room)
				break
			}
		}
	}
	return
}
