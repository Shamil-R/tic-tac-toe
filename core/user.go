package core

type (
	User struct {
		Name string
	}
)

var Users = map[string]*User{}

func NewUser(name string) *User {
	return &User{
		Name: name,
	}
}

func GetUserByName(name string) (user *User) {
	user, ok := Users[name]
	if ok {
		return
	}
	user = NewUser(name)
	Users[name] = user
	return
}
