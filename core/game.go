package core

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sort"
)

const (
	LogDir         = "log"
	FieldDimension = 3
)

type (
	// Обект игры
	Game struct {
		Id    int     `json:"id"`
		Plays []*Play `json:"plays"`
		Users []*User `json:"-"`
	}
)

// Идентификатор последней игры
var LastGameId int

// Выйгрышные позиции
var WinPosition = [][]int{
	// горизонтальные
	{0, 1, 2},
	{3, 4, 5},
	{6, 7, 8},
	// вертикальные
	{0, 3, 6},
	{1, 4, 7},
	{2, 5, 8},
	// диагонали
	{0, 4, 8},
	{2, 4, 6},
}

func NewGame(lastGameId int, username string) *Game {
	return &Game{
		Id:    lastGameId,
		Users: []*User{GetUserByName(username)},
	}
}

func CheckWinInPlaysVector(playsVector []int) bool {
	sort.Slice(playsVector, func(i, j int) bool {
		return playsVector[i] < playsVector[j]
	})
	for i := range WinPosition {
		if isSubArray(playsVector, WinPosition[i]) {
			return true
		}
	}
	return false
}

// Выявление того, что слайс A содержит в себе слайс B
func isSubArray(A []int, B []int) bool {
	// Два указателя для обхода слайсов
	i := 0
	j := 0
	// Размеры слайсов
	lenA := len(A)
	lenB := len(B)
	if lenB == 0 {
		return true
	}
	if lenA == 0 {
		return false
	}
	// Обход обоих слайсов одновременно
	for {
		// Если элементы слайсов равны
		if A[i] == B[j] {
			// Сдвигаем указатели на следующие позиции
			i++
			j++

			// Если слайс B полностью пройден, то он содержится в слайсе А
			if j == lenB {
				return true
			}
		} else {
			// Если элементы слайсов не равны, увеличиваем i и сбрасываем j
			i = i - j + 1
			j = 0
		}
		// Если вышли за границу, выходим
		if i >= lenA || j >= lenB {
			break
		}
	}
	return false
}

func (g *Game) Log(roomId int) (err error) {
	if len(g.Plays) == 0 {
		return errors.New("no plays")
	}
	path := fmt.Sprintf("%s/room_%d_game_%d.json", LogDir, roomId, g.Id)
	file, err := os.OpenFile(path, os.O_RDWR|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		return
	}
	defer file.Close()
	out, err := json.Marshal(*g.Plays[len(g.Plays)-1])
	if err != nil {
		return
	}
	var prefix, postfix string
	if len(g.Plays) == 1 {
		prefix = "["
	} else {
		prefix = ","
	}
	if g.Plays[len(g.Plays)-1].IsWin {
		postfix = "]"
	}
	_, err = fmt.Fprintf(file, "%s%s%s\n", prefix, out, postfix)
	if err != nil {
		return
	}
	return
}

func CreateGame(roomId int, userName string) (id int, err error) {
	room, ok := Rooms[roomId]
	if !ok {
		err = errors.New("Room not found")
		return
	}
	var isInRoom bool
	for _, user := range room.Users {
		if user.Name == userName {
			isInRoom = true
			break
		}
	}
	if !isInRoom {
		err = errors.New("You are not in room")
		return
	}
	LastGameId += 1
	if len(room.Games) == 0 {
		room.Games = make(map[int]*Game)
	}
	room.Games[LastGameId] = NewGame(LastGameId, userName)
	id = LastGameId
	return
}

func JoinGame(roomId, gameId int, userName string) (err error) {
	room, ok := Rooms[roomId]
	if !ok {
		err = errors.New("room not found")
		return
	}
	var isInRoom bool
	for _, user := range room.Users {
		if user.Name == userName {
			isInRoom = true
			break
		}
	}
	if !isInRoom {
		err = errors.New("you are not in room")
		return
	}
	game, ok := room.Games[gameId]
	if !ok {
		err = errors.New("game not found")
		return
	}
	if len(game.Users) == 2 {
		err = errors.New("player pool is full")
		return
	}
	for _, user := range game.Users {
		if user.Name == userName {
			err = errors.New("you already in game")
			return
		}
	}
	game.Users = append(game.Users, GetUserByName(userName))
	return
}

func PlayStep(roomId, gameId int, userName string, position int) (isWin bool, err error) {
	room, ok := Rooms[roomId]
	if !ok {
		err = errors.New("room not found")
		return
	}
	var isInRoom bool
	for _, user := range room.Users {
		if user.Name == userName {
			isInRoom = true
			break
		}
	}
	if !isInRoom {
		err = errors.New("you are not in room")
		return
	}
	game, ok := room.Games[gameId]
	if !ok {
		err = errors.New("game not found")
		return
	}
	isWin, err = game.PlayStep(userName, position)
	if err != nil {
		return
	}
	go func() {
		errr := game.Log(roomId)
		if errr != nil {
			fmt.Println(err)
		}
	}()
	return
}

func (g *Game) PlayStep(userName string, position int) (isWin bool, err error) {
	if len(g.Plays) > 0 && g.Plays[len(g.Plays)-1].IsWin {
		err = errors.New("Game finished")
		return
	}
	if position < 0 || position > FieldDimension*FieldDimension {
		err = errors.New("Position validation fail")
		return
	}
	if len(g.Users) != 2 {
		err = errors.New("player pool is not full")
		return
	}
	if len(g.Plays) > 0 && g.Plays[len(g.Plays)-1].Player == userName {
		err = errors.New("2 plays in a row is forbidden")
		return
	}
	for i := range g.Plays {
		if g.Plays[i].Position == position {
			err = errors.New("play on this position exist")
			return
		}
	}
	play := NewPlay(userName, position)
	g.Plays = append(g.Plays, play)
	if len(g.Plays) < 5 {
		return
	}
	playsVector := make([]int, 0)
	for i := range g.Plays {
		if g.Plays[i].Player == userName {
			playsVector = append(playsVector, g.Plays[i].Position)
		}
	}
	isWin = CheckWinInPlaysVector(playsVector)
	play.IsWin = isWin
	return
}
