package core

import (
	"context"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
)

const ClientKey = "client"

func (c *JwtConfig) RequestMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		header := r.Header.Get("Authorization")
		tokenString := strings.ReplaceAll(header, "Bearer ", "")
		token, err := c.Validate(tokenString)
		if err != nil {
			_, _ = fmt.Fprintln(w, err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		claims := token.Claims.(jwt.MapClaims)
		ctx := context.WithValue(r.Context(), ClientKey, claims[ClientKey])
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// Валидация токена
func (c *JwtConfig) Validate(tokenString string) (*jwt.Token, error) {
	// Проверяем на пустоту
	if tokenString == "" {
		return nil, errors.New("JWT token is required")
	}
	// Парсим токен
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(c.SecretKey), nil
	})
	// Проверяем ошибку
	if err != nil {
		return nil, err
	}
	// Разворачаваем в map[string]interface{}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New("Can't convert to MapClaims")
	}
	// Сканируем список доступных клиентов
	var isClientCorrect bool
	if token.Valid {
		client := claims[ClientKey]
		for _, service := range c.Services {
			if service == client {
				isClientCorrect = true
			}
		}
		if !isClientCorrect {
			return nil, errors.New("Client is incorrect or not exists in allowed list")
		} else {
			return token, nil
		}
	}
	return nil, errors.New("JWT Token is invalid")
}
