package core

import "testing"

func TestIsSubArray(t *testing.T) {
	a := []int{}
	b := []int{}
	if !isSubArray(a, b) {
		t.Fatal("Пустой массив входит в пустой массив")
	}
	b = append(b, 1)
	if isSubArray(a, b) {
		t.Fatal("Не пустой массив не входит в пустой массив")
	}
	a = append(a, 2)
	a = append(a, 3)
	if isSubArray(a, b) {
		t.Fatal("Ошибка определения вхождения массива")
	}
	a = append(a, 1)
	if !isSubArray(a, b) {
		t.Fatal("Ошибка определения вхождения массива")
	}
	a = append(a, 4)
	b = append(b, 4)
	if !isSubArray(a, b) {
		t.Fatal("Ошибка определения вхождения массива")
	}
}

func TestCheckWinInPlaysVector(t *testing.T) {
	vector := []int{0, 3, 4}
	if CheckWinInPlaysVector(vector) {
		t.Fatal("Не выйгрышная позиция определена как выйгрышная")
	}
	vector = append(vector, 5)
	if !CheckWinInPlaysVector(vector) {
		t.Fatal("Выйгрышная позиция определена как не выйгрышная")
	}
}

func TestGame(t *testing.T) {
	roomId, err := CreateRoom(ClientNameRafael)
	if err != nil {
		t.Fatal(err)
	}
	// Rodjer
	err = JoinRoom(roomId, ClientNameRodjer)
	if err != nil {
		t.Fatal(err)
	}
	gameId, err := CreateGame(roomId, ClientNameRafael)
	if err != nil {
		t.Fatal(err)
	}
	_, err = PlayStep(roomId, gameId, ClientNameRafael, 0)
	if err == nil {
		t.Fatal("Не определена ошибка при попытке сделать ход без второго игрока")
	}
	err = JoinGame(roomId, gameId, ClientNameRodjer)
	if err != nil {
		t.Fatal(err)
	}
	// game
	isWin, err := PlayStep(roomId, gameId, ClientNameRafael, 0)
	if err != nil {
		t.Fatal(err)
	}
	if isWin {
		t.Fatal("Неверное определение победителя")
	}
	isWin, err = PlayStep(roomId, gameId, ClientNameRafael, 1)
	if err == nil {
		t.Fatal("Повторно ходить нельзя")
	}
	if isWin {
		t.Fatal("Неверное определение победителя")
	}
	isWin, err = PlayStep(roomId, gameId, ClientNameRodjer, 9)
	if err != nil {
		t.Fatal(err)
	}
	if isWin {
		t.Fatal("Неверное определение победителя")
	}
	isWin, err = PlayStep(roomId, gameId, ClientNameRafael, 1)
	if err != nil {
		t.Fatal(err)
	}
	if isWin {
		t.Fatal("Неверное определение победителя")
	}
	isWin, err = PlayStep(roomId, gameId, ClientNameRodjer, 8)
	if err != nil {
		t.Fatal(err)
	}
	if isWin {
		t.Fatal("Неверное определение победителя")
	}
	isWin, err = PlayStep(roomId, gameId, ClientNameRafael, 2)
	if err != nil {
		t.Fatal(err)
	}
	if !isWin {
		t.Fatal("Неверное определение победителя")
	}
}
