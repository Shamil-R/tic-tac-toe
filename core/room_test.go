package core

import "testing"

func TestRoom(t *testing.T) {
	id, err := CreateRoom(ClientNameRafael)
	if err != nil {
		t.Fatal(err)
	}
	if id != 1 {
		t.Fatal("Неверный идентификатор первой комнаты")
	}
	id, err = CreateRoom(ClientNameRafael)
	if err != nil {
		t.Fatal(err)
	}
	if id != 2 {
		t.Fatal("Неверный идентификатор второй комнаты")
	}
	rooms := GetRooms(ClientNameRafael)
	for i := range rooms {
		if rooms[i].Id != i+1 {
			t.Fatal("Неверный идентификатор комнаты")
		}
		if rooms[i].Users[0].Name != ClientNameRafael {
			t.Fatal("Создатель команы должен сразу попадать в комнату")
		}
	}
	// Rodjer
	err = JoinRoom(id, ClientNameRodjer)
	if err != nil {
		t.Fatal(err)
	}
	rooms = GetRooms(ClientNameRodjer)
	for i := range rooms {
		if rooms[i].Users[1].Name != ClientNameRodjer {
			t.Fatal("Присоединение к комнате не работает")
		}
	}
}
