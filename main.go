package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"tic-tac-toe/core"
)

type (
	FormPosition struct {
		Position int `json:"position"`
	}
)

func CreateRoom(w http.ResponseWriter, r *http.Request) {
	userName := r.Context().Value(core.ClientKey)
	id, err := core.CreateRoom(userName.(string))
	if err != nil {
		fmt.Fprintf(w, "failed create room: %s\n", err)
		return
	}
	fmt.Fprintf(w, "room created: %d\n", id)
}

func GetRooms(w http.ResponseWriter, r *http.Request) {
	userName := r.Context().Value(core.ClientKey)
	rooms := core.GetRooms(userName.(string))
	fmt.Fprintf(w, "List:\n")
	for i := range rooms {
		fmt.Fprintf(w, "%d: %+v\n", rooms[i].Id, rooms[i])
	}
}

func JoinRoom(w http.ResponseWriter, r *http.Request) {
	userName := r.Context().Value(core.ClientKey)
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)
	if err != nil {
		fmt.Fprintf(w, "bad room id: %s\n", err)
		return
	}
	err = core.JoinRoom(int(id), userName.(string))
	if err != nil {
		fmt.Fprintf(w, "failed join room: %s\n", err)
		return
	}
	fmt.Fprintf(w, "success\n")

}

func Play(w http.ResponseWriter, r *http.Request) {
	userName := r.Context().Value(core.ClientKey)
	roomId, err := strconv.ParseInt(mux.Vars(r)["roomId"], 10, 64)
	if err != nil {
		fmt.Fprintf(w, "bad room id: %s\n", err)
		return
	}
	gameId, err := strconv.ParseInt(mux.Vars(r)["gameId"], 10, 64)
	if err != nil {
		fmt.Fprintf(w, "bad game id: %s\n", err)
		return
	}
	position := &FormPosition{}
	err = ParseJsonBody(r.Body, position)
	if err != nil {
		fmt.Fprintf(w, "failed pars form: %s\n", err)
		return
	}
	isWin, err := core.PlayStep(int(roomId), int(gameId), userName.(string), position.Position)
	if err != nil {
		fmt.Fprintf(w, "fail: %s\n", err)
		return
	}
	if isWin {
		fmt.Fprintf(w, "you win!\n")
	} else {
		fmt.Fprintf(w, "success\n")
	}
}

// Прочитать тело дескриптор запроса
func ParseJsonBody(r io.ReadCloser, data interface{}) (err error) {
	body, err := ioutil.ReadAll(r)
	if err != nil {
		return
	}
	defer func() {
		err := r.Close()
		if err != nil {
			fmt.Println(err)
		}
	}()
	err = json.Unmarshal(body, data)
	if err != nil {
		return
	}
	return nil
}

func CreateGame(w http.ResponseWriter, r *http.Request) {
	userName := r.Context().Value(core.ClientKey)
	roomId, err := strconv.ParseInt(mux.Vars(r)["roomId"], 10, 64)
	if err != nil {
		fmt.Fprintf(w, "bad room id: %s\n", err)
		return
	}
	id, err := core.CreateGame(int(roomId), userName.(string))
	if err != nil {
		fmt.Fprintf(w, "failed create game: %s\n", err)
		return
	}
	fmt.Fprintf(w, "game created: %d\n", id)
}

func JoinGame(w http.ResponseWriter, r *http.Request) {
	userName := r.Context().Value(core.ClientKey)
	roomId, err := strconv.ParseInt(mux.Vars(r)["roomId"], 10, 64)
	if err != nil {
		fmt.Fprintf(w, "bad room id: %s\n", err)
		return
	}
	gameId, err := strconv.ParseInt(mux.Vars(r)["gameId"], 10, 64)
	if err != nil {
		fmt.Fprintf(w, "bad game id: %s\n", err)
		return
	}
	err = core.JoinGame(int(roomId), int(gameId), userName.(string))
	if err != nil {
		fmt.Fprintf(w, "failed join game: %s\n", err)
		return
	}
	fmt.Fprintf(w, "success\n")

}

func main() {
	// Роуты
	r := mux.NewRouter()
	MainRoute := r.PathPrefix("/tic-tak-toe").Subrouter()
	room := MainRoute.PathPrefix("/room").Subrouter()
	room.HandleFunc("", CreateRoom).Methods("POST")
	room.HandleFunc("/all", GetRooms).Methods("GET")
	room.HandleFunc("/{id:[0-9]+}/join", JoinRoom).Methods("POST")
	room.HandleFunc("/{roomId:[0-9]+}/game/{gameId:[0-9]+}/play", Play).Methods("POST")
	room.HandleFunc("/{roomId:[0-9]+}/game", CreateGame).Methods("POST")
	room.HandleFunc("/{roomId:[0-9]+}/game/{gameId:[0-9]+}/join", JoinGame).Methods("POST")
	// Мидлварина для проверки токена
	jwtConfig := core.NewJwtConfig()
	r.Use(jwtConfig.RequestMiddleware)
	// Старт сервиса
	port := os.Getenv("TTK_PORT")
	fmt.Printf("Start web server at 0.0.0.0:%s\n", port)
	err := http.ListenAndServe(":"+port, r)
	if errors.Is(err, http.ErrServerClosed) {
		fmt.Printf("server closed\n")
	} else if err != nil {
		fmt.Printf("error listening for server: %s\n", err)
	}
}
